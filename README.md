# Roboccino interfaces

The repository contains the definition of interfaces used in the [Roboccino](https://gitlab.com/roboccino/roboccino) project. It defines three types of messages:
- *PowderDispenserParams.msg*: rounds per minute and dispenser-opening number of steps of a stepper motor being a part of a powder dispenser;
- *StirringParams.msg*: stirring speed and time of a coffee stirrer;
- *WaterDispenserParams.msg*: opening time and definitions of closed and open water valve values for a water dispenser containing a servo.
